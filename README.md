##This repo contains initial testing on 3 companies (WLL, CLRS & COUR).
The aim of the test is to forecast Revenue using Average commodity prices
and production volumes sourced form IBES, the calculated surprise is then
compared to surprise of sell-side estimated Revenue against Actual reported
Absolute mean average of both surprises (sell-side versus calculated) is
used to determine the effectiveness of this method.

##Results are available in https://dataffirm.atlassian.net/browse/DS-36

query = """
SELECT * from (
  SELECT
    EstPermID,
    IBESTicker,
    Measure,
    PerType,
    PerEndDate,
    convert(char(10), AnnounceDate, 126) AnnounceDate,
    DefActValue,
    DefActSurprise
  FROM

    (
      SELECT *
      FROM (
             SELECT
               EstPermID,
               IBESTicker,
               Measure,
               PerType,
               PerEndDate,
               FYEMonth,
               DefActSurprise,
               RegionPermID
             FROM (SELECT *
                   FROM (SELECT *
                         FROM qai.dbo.treactsurpwin
                         WHERE WinLength = 0
                        ) temp1
                     LEFT JOIN
                     (
                       SELECT
                         estpermid permid,
                         IBESTicker
                       FROM qai.dbo.treinfo
                     ) temp2
                       ON temp1.EstPermID = permid
                  ) temp3
           ) temp4
      WHERE Measure IN (8, 9, 15, 20) AND PerType = 3 AND PerEndDate > '1995'
    ) temp5
    LEFT JOIN
    (
      SELECT
        EstPermID  ann_id,
        Measure    ann_measure,
        PerEndDate ann_perend,
        FYEMonth   ann_fymonth,
        AnnounceDate,
        DefActValue
      FROM qai.dbo.TREActRpt
      WHERE Measure IN (8, 9, 15, 20) AND PerType = 3
      --WHERE EstPermID = '30064842587' AND Measure IN (8,9) AND PerType = 3
    ) ann ON ann_id = temp5.estpermid AND ann_measure = temp5.Measure AND ann_perend = temp5.perenddate and ann.ann_id = temp5.EstPermID
) announcement order by PerEndDate
"""